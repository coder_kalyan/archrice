set nocompatible " we don't do vi compatibility around here

" Quickly edit/reload the vimrc file
nmap <silent> <leader>ev :e $MYVIMRC<CR>
nmap <silent> <leader>sv :so $MYVIMRC<CR>

set nu rnu " hybrid line numbers
set incsearch " higlight search while typing
set nohlsearch
set smartcase     " ignore case if search pattern is all lowercase,
                    "    case-sensitive otherwise
set shiftround    " use multiple of shiftwidth when indenting with '<' and '>'
set showmatch     " set show matching parenthesis
set nowrap        " don't wrap lines
set autoindent    " always set autoindenting on
set copyindent    " copy the previous indentation on autoindenting
set visualbell           " don't beep
set noerrorbells         " don't beep
set title " change the terminal title to reflect vim/file
set mouse=a " mouse scrolling is nice, although don't overuse it!
set hidden " hide buffers, don't close them

" persistent undo saves history even when you close the buffer
set undodir="~/.config/nvim/undodir"
set undofile

set redrawtime=10000 " fix syntax highlight on large files
"set t_8f=\[[38;2;%lu;%lu;%lum
"set t_8b=\[[48;2;%lu;%lu;%lum

cmap w!! w sudo tee % >/dev/null " we tend to forget sudo when editing protected files
" Spacebar as leader, not move forward
nnoremap <SPACE> <Nop>
let mapleader=" "

nnoremap Y y$ " Y should yank to end of line

" when inserting quotes/braces, move cursor between them automatically
"inoremap <> <><Left>
"inoremap () ()<Left>
"inoremap {} {}<Left>
"inoremap [] []<Left>
"inoremap "" ""<Left>
"inoremap '' ''<Left>
"inoremap `` ``<Left>

" Saner navigation binds
noremap K     {
noremap J     }
noremap H     ^
noremap L     $

" even emacs is better than the arrow keys...
inoremap <C-p> <Up>
inoremap <C-n> <Down>
inoremap <C-b> <Left>
inoremap <C-f> <Right>

" (Shift) TAB for indent
nnoremap <Tab>   >>
nnoremap <S-Tab> <<
vnoremap <Tab>   >><Esc>gv
vnoremap <S-Tab> <<<Esc>gv

nnoremap Q @q " a slightly easier way to run macros

" tab config
set tabstop=4
set shiftwidth=4
set expandtab
"autocmd filetype yaml setlocal tabstop=2 shiftwidth=2
filetype plugin indent on
set omnifunc=syntaxcomplete#Complete

call plug#begin('~/.local/share/nvim/plugged') " vim-plug plugin manager

Plug 'Shougo/neco-vim'
Plug 'neoclide/coc-neco'
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'tjdevries/coc-zsh'

Plug 'honza/vim-snippets' " Popular snippet files

Plug 'drewtempelmeyer/palenight.vim' " color scheme

Plug 'scrooloose/nerdcommenter' " comment helper

Plug 'majutsushi/tagbar' " tag outlines

Plug 'kien/ctrlp.vim' " fuzzy file finder

Plug 'tpope/vim-fugitive' " git plugin

Plug 'scrooloose/nerdtree' " tree view of directory

Plug 'mboughaba/i3config.vim' " i3 config syntax highlighting

Plug 'vim-airline/vim-airline' " nice statusline
Plug 'vim-airline/vim-airline-themes' " and its themes

Plug 'sheerun/vim-polyglot' " language addon

"Plug 'ludovicchabant/vim-gutentags' " ctags

call plug#end()

" NCM2
"au BufEnter * call ncm2#enable_for_buffer()
"set completeopt=noinsert,menuone,noselect
"inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"set shortmess+=c

"set -ga terminal-overrides ',xterm-256color:Tc'

" palenight color scheme + a few modifications
set background=dark
colorscheme palenight
set termguicolors " 24 bit truecolor
highlight Comment ctermfg=100
highlight LineNr ctermfg=223
highlight CursorLineNr ctermfg=223
highlight Comment cterm=italic gui=italic

" transparent background
highlight Normal ctermbg=none guibg=none
highlight NonText ctermbg=none guibg=none
highlight StatusLine ctermbg=none cterm=bold guibg=none
hi Statement          ctermbg=none guibg=none
hi Title              ctermbg=none guibg=none
hi Todo               ctermbg=none guibg=none
hi Underlined         ctermbg=none guibg=none
hi ErrorMsg           ctermbg=none guibg=none
hi LineNr             ctermbg=none guibg=none
highlight airline_c ctermbg=none guibg=none
highlight airline_tabfill ctermbg=none guibg=none

" UltiSnips configuration
"inoremap <silent> <expr> <CR> ncm2_ultisnips#expand_or("\<CR>", 'n')
"let g:UltiSnipsJumpForwardTrigger	= "<c-j>"
"let g:UltiSnipsJumpBackwardTrigger	= "<c-k>"
"let g:UltiSnipsRemoveSelectModeMappings = 0

nmap <leader>t :TagbarToggle<CR>

"au BufWritePost,BufEnter,InsertLeave * Neomake

" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='deus'
set noshowmode " airline shows us the mode, so no need for '-- INSERT --'

" Disable Jedi-vim autocompletion and enable call-signatures options
"let g:jedi#auto_initialization = 1
"let g:jedi#completions_enabled = 0
"let g:jedi#auto_vim_configuration = 0
"let g:jedi#smart_auto_mappings = 0
"let g:jedi#popup_on_dot = 0
"let g:jedi#completions_command = ""
"let g:jedi#show_call_signatures = "1"

" browserlink needs some configuration to work well with Jekyll...
" Disable built-in event handling...
"let g:bl_no_autoupdate = 1

"" ...in lieu of custom event handler to force delay inside Jekyll directories.
"let s:delay_interval = '1000m'
"let s:bl_pagefileexts  = 
      "\ [ 'html' , 'js'     , 'php'  ,
      "\   'css'  , 'scss'   , 'sass' ,
      "\   'slim' , 'liquid' , 'md'   ,
      "\   'yml' ]

"function! s:setupHandlers()
  "let s:path_flag = '%:p:h' | let s:this_path = expand(s:path_flag)
  "while s:this_path != $HOME 
    "if !empty(globpath(s:this_path,'_config.yml')) 
      "exec 'sleep ' . s:delay_interval | break 
    "endif 
    "let s:path_flag .= ':h' | let s:this_path = expand(s:path_flag) 
  "endwhile 
  "if expand('%:e:e') =~ 'css' 
    ":BLReloadCSS 
  "else
    ":BLReloadPage 
  "endif
"endfunction

"augroup browserlink
  "autocmd!
  "exec 'autocmd BufWritePost *.' . join(s:bl_pagefileexts, ',*.') . ' call s:setupHandlers()'
"augroup END

"" Set native comment markers in Liquid files
"let liquid_ext = expand('%:e:e')
"if liquid_ext =~ '\(ht\|x\)ml'
  "set commentstring=<!--%s-->
"elseif liquid_ext =~ 'css'
  "set commentstring=/*%s*/
"endif


" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1] =~# '\s'
endfunction

let g:coc_snippet_next = '<tab>'

inoremap <silent><expr> <Tab>
    \ pumvisible() ? coc#_select_confirm() :
    \ coc#expandableOrJumpable() ? "\<C-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
    \ <SID>check_back_space() ? "\<TAB>" :
    \ coc#refresh()

set cmdheight=2 " better display for messages
set updatetime=300
set shortmess+=c " don't give |ins-completion-menu| messages
set signcolumn=yes " always show signcolumns

" use tab for trigger completion with characters ahead and navigate
inoremap <silent><expr> <TAB>
        \ pumvisible() ? "\<C-n>" :
        \ <SID>check_back_space() ? "\<TAB>" :
        \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1] =~# '\s'
endfunction

" use <c-space> to trigger completion
inoremap <silent><expr> <c-space> coc#refresh()

" use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position
" coc only does snippet and additional edit on confirm
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documetation()<CR>

function! s:show_documetation()
    if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
    else
        call CocAction('doHover')
    endif
endfunction

autocmd CursorHold * silent call CocActionAsync('highlight')

" remap for format selected region
xmap <leader>f <Plug>(coc-format-selected)
nmap <leader>f <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use <tab> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <S-TAB> <Plug>(coc-range-select-backword)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

nnoremap <silent> <space>y :<C-u>CocList -A --normal yank<cr>

"if has('cscope')
  "set cscopetag cscopeverbose

  "if has('quickfix')
    "set cscopequickfix=s-,c-,d-,i-,t-,e-
  "endif

  "cnoreabbrev csa cs add
  "cnoreabbrev csf cs find
  "cnoreabbrev csk cs kill
  "cnoreabbrev csr cs reset
  "cnoreabbrev css cs show
  "cnoreabbrev csh cs help

  "command -nargs=0 Cscope cs add $VIMSRC/src/cscope.out $VIMSRC/src
"endif

